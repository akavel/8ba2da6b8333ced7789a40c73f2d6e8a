{ nixpkgs ? import <nixpkgs> {} }:
with nixpkgs;
pkgs.symlinkJoin {
  name = "myapps";
  paths = with pkgs; [
    sqlite ripgrep
  ];
}
